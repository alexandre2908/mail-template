# Mail template 

the original creation was designed by : [Adele *aka-Ozzizo*](https://dribbble.com/shots/6072133-mail-dashboard-for-deviantart)

![](Assets/img/mail_template.png)

in this repo i have recreated all this page using **HTML**, **css**, and **tailwindcss**

## For using the project
to launch the project just clone this repository 
```bash
git clone https://gitlab.com/alexandre2908/mail-template.git
```
navigate to the folder of the clone you've made and then launch index.html

if you want to continue or adding some usefull page or design just run

```bash
npm install
```
Make sure you have [NodeJs](https://nodejs.org) install on your computer

and then you can start adding some design :)
